<?php

namespace App\Http\Controllers;

use App\Penjualan;
use App\Barang;
use App\Pelanggan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Redirect;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjualan = DB::table('penjualan')
            ->select('penjualan.id', 'penjualan.no_penjualan', 'penjualan.tanggal', 'penjualan.kode_pelanggan', 
            'pelanggan.nama_pelanggan', 'penjualan.kode_barang', 'barang.nama_barang', 
            'penjualan.jumlah_barang', 'penjualan.harga_barang')
            ->join('pelanggan',  'penjualan.kode_pelanggan', '=', 'pelanggan.kode_pelanggan')
            ->join('barang', 'penjualan.kode_barang', '=', 'barang.kode_barang')
            ->where([
                ['penjualan.jumlah_barang', '>=', 10],
                ['penjualan.harga_barang', '>=', 10000],
        ])
        ->paginate(5);
    return view('penjualan.index', compact('penjualan'))->with('i', (request()->input('page', 1) -1));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = Barang::all();
        $pelanggan = Pelanggan::all();
        return view('penjualan.create', compact('barang', 'pelanggan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'no_penjualan' => [
                'required',
                Rule::unique('penjualan')->ignore($request->input('id_penjualan'))
            ],
            'tanggal' => 'required',
            'kode_pelanggan' => 'required',
            'nama_pelanggan' => 'required',
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'jumlah_barang' => 'required',
            'harga_barang' => 'required',
        ]);

        $barang = Barang::find($request->input('kode_barang'));
        if(!$barang) {
            return redirect()->back()->withInput()->with('error', 'Data Barang tidak ditemukan');
        }
        if ($request->input('jumlah_barang')<= 0) {
            return redirect()->back()->withInput()->with('error', 'Jumlah Barang harus lebih besar dari 0');
        }
        if ($barang->stok_barang <= $request->input('jumlah_barang')) {
            return redirect()->back()->withInput()->with('error', 'Stok Barang Tidak Cukup');
        }
        else{
            // Menyimpan nama User setiap dalam penginputan
            $penjualan = new Penjualan($validateData);
            $penjualan->created_by = Auth::user()->name;
            $penjualan->save();
            $barang->stok_barang -= $request->input('jumlah_barang');
            $barang->save();
            return redirect()->route('penjualan.index')->with('success','penjualan created Successfully');
        }
   
        return redirect()->route('penjualan.index')->with('success','penjualan created successfully.');    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show(Penjualan $penjualan)
    {
        return view('penjualan.show', compact('penjualan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjualan $penjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjualan $penjualan)
    {
        //
    }
}
