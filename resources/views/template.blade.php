<!DOCTYPE html>
<html lang="en">
<head>
    <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UTS FULLSTACK</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>
<body style="background: lightgray">
    <div class="container" >
        <div class="container card-body">
            <a href="{{ url('/') }}" class="btn btn-sm btn-primary">Home</a>
            <a href="{{ route('barang.index') }}" class="btn btn-sm btn-primary">Data Barang</a>
            <a href="{{ route('pelanggan.index') }}" class="btn btn-sm btn-primary">Data Pelanggan</a>
            <a href="{{ route('penjualan.index') }}" class="btn btn-sm btn-primary">Data Penjualan</a>
            <a href="{{ route('pembelian.index') }}" class="btn btn-sm btn-primary">Data Pembelian</a>
            <a href="{{ route('supplier.index') }}" class="btn btn-sm btn-primary">Data Supplier</a>
        </div>
        <br>
        <div class="container">
            @yield('content')
        </div>
    </div>
</body>
</html>