@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Barang</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('pelanggan.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('pelanggan.update',$pelanggan->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kode Pelanggan:</strong>
                <input type="text" maxlength="10" name="kode_pelanggan" 
                class="form-control" placeholder="KODE PELANGGAN" value="{{ $pelanggan->kode_pelanggan }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Pelanggan:</strong>
                <input type="text" name="nama_pelanggan" 
                class="form-control" placeholder="NAMA PELANGGAN" value="{{ $pelanggan->nama_pelanggan }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                <textarea class="form-control" style="height:150px" name="alamat" placeholder="Content">{{ $pelanggan->alamat }}</textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kota:</strong>
                <input type="text" name="nama_kota" class="form-control" placeholder="Serpong BSD" value="{{ $pelanggan->nama_kota }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Telepone:</strong>
                <input type="text" name="no_telepon" class="form-control" placeholder="100.000" value="{{ $pelanggan->no_telepon }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>

    </form>
@endsection